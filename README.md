**Repository location moved! Please check [https://github.com/MaastrichtU-CDS/projects_dash-project_add-annotations](https://github.com/MaastrichtU-CDS/projects_dash-project_add-annotations)**
Run the following command before running the main file to install all the required libraries:
pip install -r requirements.txt

This script adds a new annotations graph for the Output.ttl file to the SPARQL endpoint.
